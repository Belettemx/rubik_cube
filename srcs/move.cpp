#include "move.hpp"
using namespace std;

Move::Move(char c){
  // cout << "Move default constructor" << endl;
  switch (c) {
    case 'U':
      this->cornerPerm = { Corners::UBR, Corners::URF, Corners::UFL, Corners::ULB, Corners::DFR, Corners::DLF, Corners::DBL, Corners::DRB };
      this->cornerOrientation = { 0, 0, 0, 0, 0, 0, 0, 0 };
      this->edgePerm = { Edges::UB, Edges::UR, Edges::UF, Edges::UL, Edges::DR, Edges::DF, Edges::DL, Edges::DB, Edges::FR, Edges::FL, Edges::BL, Edges::BR };
      this->edgeOrientation = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
      break;
    case 'R':
      this->cornerPerm = { Corners::DFR, Corners::UFL, Corners::ULB, Corners::URF, Corners::DRB, Corners::DLF, Corners::DBL, Corners::UBR};
      this->cornerOrientation = { 2, 0, 0, 1, 1, 0, 0, 2 };
      this->edgePerm = { Edges::FR, Edges::UF, Edges::UL, Edges::UB, Edges::BR, Edges::DF, Edges::DL, Edges::DB, Edges::DR, Edges::FL, Edges::BL, Edges::UR };
      this->edgeOrientation = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
      break;
    case 'F':
      this->cornerPerm = { Corners::UFL, Corners::DLF, Corners::ULB, Corners::UBR, Corners::URF, Corners::DFR, Corners::DBL,Corners:: DRB};
      this->cornerOrientation = { 1, 2, 0, 0, 2, 1, 0, 0 };
      this->edgePerm = { Edges::UR, Edges::FL, Edges::UL, Edges::UB, Edges::DR, Edges::FR, Edges::DL, Edges::DB, Edges::UF, Edges::DF, Edges::BL, Edges::BR };
      this->edgeOrientation = { 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0 };
      break;
    case 'D':
      this->cornerPerm = { Corners::URF, Corners::UFL, Corners::ULB, Corners::UBR, Corners::DLF, Corners::DBL, Corners::DRB, Corners::DFR};
      this->cornerOrientation = { 0, 0, 0, 0, 0, 0, 0, 0 };
      this->edgePerm = { Edges::UR, Edges::UF, Edges::UL, Edges::UB, Edges::DF, Edges::DL, Edges::DB, Edges::DR, Edges::FR, Edges::FL, Edges::BL, Edges::BR };
      this->edgeOrientation = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
      break;
    case 'L':
      this->cornerPerm = { Corners::URF, Corners::ULB, Corners::DBL, Corners::UBR, Corners::DFR, Corners::UFL, Corners::DLF, Corners::DRB};
      this->cornerOrientation = { 0, 1, 2, 0, 0, 2, 1, 0 };
      this->edgePerm = { Edges::UR, Edges::UF, Edges::BL, Edges::UB, Edges::DR, Edges::DF, Edges::FL, Edges::DB, Edges::FR, Edges::UL, Edges::DL, Edges::BR };
      this->edgeOrientation = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
      break;
    case 'B':
      this->cornerPerm = { Corners::URF, Corners::UFL, Corners::UBR, Corners::DRB, Corners::DFR, Corners::DLF, Corners::ULB, Corners::DBL};
      this->cornerOrientation = { 0, 0, 1, 2, 0, 0, 2, 1 };
      this->edgePerm = { Edges::UR, Edges::UF, Edges::UL, Edges::BR, Edges::DR, Edges::DF, Edges::DL, Edges::BL, Edges::FR, Edges::FL, Edges::UB, Edges::DB };
      this->edgeOrientation = { 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1 };
      break;
  }
}

Move::~Move() {
  // cout << "Move destructor" << endl;
}
