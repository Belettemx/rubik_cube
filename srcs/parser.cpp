#include "parser.hpp"

using namespace std;


Parser::Parser(string arg, int nbrMoves) {
  this->_hash["U"]  = {{0, 1}};
  this->_hash["U2"] = {{0, 2}};
  this->_hash["U'"] = {{0, 3}};
  this->_hash["R"]  = {{1, 1}};
  this->_hash["R2"] = {{1, 2}};
  this->_hash["R'"] = {{1, 3}};
  this->_hash["F"]  = {{2, 1}};
  this->_hash["F2"] = {{2, 2}};
  this->_hash["F'"] = {{2, 3}};
  this->_hash["D"]  = {{3, 1}};
  this->_hash["D2"] = {{3, 2}};
  this->_hash["D'"] = {{3, 3}};
  this->_hash["L"]  = {{4, 1}};
  this->_hash["L2"] = {{4, 2}};
  this->_hash["L'"] = {{4, 3}};
  this->_hash["B"]  = {{5, 1}};
  this->_hash["B2"] = {{5, 2}};
  this->_hash["B'"] = {{5, 3}};

  FaceCube f;
  cubieCube = f.toCubieCube();

  if (!arg.empty()){
    vector<string> movesStrings = split(arg, ' ');
    movesToInt(movesStrings);
  } else {
    randomMoves(nbrMoves);
    printMoves();
  }

  applyMoves();
}

Parser::~Parser(){
}

void Parser::randomMoves(int nbrMoves) {
  cout << "======================= Random Moves =======================" <<endl;

  for (int i = nbrMoves; i > 0; i--)
  {
    vector<int> v(2);
    v[0] = rand() % 6;
    v[1] = rand() % 3 + 1;
    userMoves.push_back(v);
  }

}


void         Parser::split(const string &s, char delim, vector<string> &elems) {
    stringstream ss;
    ss.str(s);
    string item;
    while (getline(ss, item, delim)) {
      if (!item.empty()) {
        elems.push_back(item);
      }
    }
}

vector<string>  Parser::split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

void            Parser::movesToInt(vector<string> movesStrings) {
  for (vector<string>::iterator it = movesStrings.begin() ; it != movesStrings.end(); ++it) {
    map<std::string, vector<int>>::iterator  it_bis = this->_hash.find(*it);
    if (it_bis != this->_hash.end()) {
      this->userMoves.push_back(it_bis->second);
    } else {
      throw invalid_argument("Move undefined: " + *it);
    }
  }
}



void Parser::applyMoves() {
  vector<Move> moves = getMoves();

  for (size_t i = 0; i < userMoves.size(); i++) {
    for (int j = 0; j < userMoves[i][1]; j++) {
      cubieCube.edgeMultiply(moves[userMoves[i][0]]);
      cubieCube.cornerMultiply(moves[userMoves[i][0]]);
    }
  }
}

string Parser::toString() {
  return cubieCube.toFaceCube().toString();
}

char Parser::toColorChar(int x) {
      switch((Colors)x) {
          case Colors::U :
              return ('U');
              break;
          case Colors::R :
              return ('R');
              break;
          case Colors::F :
              return ('F');
              break;
          case Colors::D :
              return ('D');
              break;
          case Colors::L :
              return ('L');
              break;
          case Colors::B :
              return ('B');
              break;
      }
}

char Parser::toMovementChar(int x){
  switch (x) {
    case 2:
      return '2';
    case 3:
      return '\'';
    default:
      return ' ';
  }
}

void Parser::printMoves() {
  cout << userMoves.size() << " randomly generated moves:" << endl;
  for (size_t i = 0; i < userMoves.size(); i++){

    cout << toColorChar(userMoves[i][0]) << toMovementChar(userMoves[i][1]) << " ";
  }
  cout << endl ;
}
