#include "solver.hpp"
#include <chrono>
using namespace std;

Solver::Solver(): fc(*new FaceCube()), cc(*new CubieCubeLvl()), c(*new CoordLvl()){
  // cout << "Solver default constructor" << endl;
}

Solver::~Solver() {
  // cout << "Solver destructor" << endl;
}

signed char Solver::min(signed char a, signed char b){
  return a < b ? a : b;
}

signed char Solver::max(signed char a, signed char b){
  return a > b ? a : b;
}

string Solver::solution(string facelets, int maxDepth, long timeOut, int useSeparator, CoordLvl &c, FaceCube &fc, CubieCubeLvl &cc) {
  int s;

  // +++++++++++++++ check for wrong input +++++++++++++++++++++++
  array<int, 6> counts;
  counts[(int)Colors::U] = count(facelets.begin(), facelets.end(), 'U');
  counts[(int)Colors::R] = count(facelets.begin(), facelets.end(), 'R');
  counts[(int)Colors::F] = count(facelets.begin(), facelets.end(), 'F');
  counts[(int)Colors::D] = count(facelets.begin(), facelets.end(), 'D');
  counts[(int)Colors::L] = count(facelets.begin(), facelets.end(), 'L');
  counts[(int)Colors::B] = count(facelets.begin(), facelets.end(), 'B');

  for (unsigned long i = 0; i < counts.size(); i++){
    if (counts[i] != 9){
      return NULL;
    }
  }

  this->fc = fc;

  //this->cc = *new CubieCubeLvl(this->fc);
  this->cc = cc;
  if (this->cc.verify() != 0) {
    return NULL;
  }

  // +++++++++++++++++ initialization +++++++++++++++++++++++++++
  this->c = c;

  this->po[0] = 0;
  this->ax[0] = 0;
  this->flip[0] = this->c.flip;
  this->twist[0] = this->c.twist;
  this->parity[0] = this->c.parity;
  this->slice[0] = this->c.FRtoBR / 24;
  this->URFtoDLF[0] = this->c.URFtoDLF;
  this->FRtoBR[0] = this->c.FRtoBR;
  this->URtoUL[0] = this->c.URtoUL;
  this->UBtoDF[0] = this->c.UBtoDF;
  this->minDistPhase1[1] = 1;// else failure for depth=1, n=0
  int mv = 0, n = 0;
  int busy = 0;
  int depthPhase1 = 1;

  time_t tStart = time(NULL);
  auto start = std::chrono::steady_clock::now();

  // +++++++++++++++++++ Main loop ++++++++++++++++++++++++++++++++++++++++++
  while (true) {
    do {
      if ((depthPhase1 - n > this->minDistPhase1[n + 1]) && !busy) {
        if (this->ax[n] == 0 || this->ax[n] == 3){// Initialize next move
          this->ax[++n] = 1;
        } else {
          this->ax[++n] = 0;
        }
        this->po[n] = 1;
      } else if (++this->po[n] > 3) {

        do {
          // increment axis
          if (++this->ax[n] > 5) {
            if (time(NULL) - tStart > timeOut){
              return NULL;
            }
            if (n == 0) {
              if (depthPhase1 >= maxDepth){
                return NULL;
              } else {
                depthPhase1++;
                this->ax[n] = 0;
                this->po[n] = 1;
                busy = 0;
                break;
              }
            } else {
              n--;
              busy = 1;
              break;
            }
          } else {
            this->po[n] = 1;
            busy = 0;
          }
        } while (
          n != 0
          && (this->ax[n - 1] == this->ax[n]
            || this->ax[n - 1] - 3 == this->ax[n]
          )
        );
      } else {
        busy = 0;
      }
    } while (busy);

    // +++++++++++++ compute new coordinates and new minDistPhase1 ++++++++++
    // if minDistPhase1 =0, the H subgroup is reached
    mv = 3 * this->ax[n] + this->po[n] - 1;
    this->flip[n + 1] = this->c.flipMove[this->flip[n]][mv];
    this->twist[n + 1] = this->c.twistMove[this->twist[n]][mv];
    this->slice[n + 1] = this->c.FRtoBR_Move[this->slice[n] * 24][mv] / 24;

    this->minDistPhase1[n + 1] = max(
      c.getPruning(c.Slice_Flip_Prun, N_SLICE1 * flip[n + 1] + slice[n + 1]),
      c.getPruning(c.Slice_Twist_Prun, N_SLICE1 * twist[n + 1] + slice[n + 1])
    );
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // System.out.format("%d %d\n", n, depthPhase1);
    if (this->minDistPhase1[n + 1] == 0 && n >= depthPhase1 - 5) {

      this->minDistPhase1[n + 1] = 10;// instead of 10 any value >5 is possible
      if (n == depthPhase1 - 1 && (s = totalDepth(depthPhase1, maxDepth)) >= 0) {
        if (
          s == depthPhase1
          || (
            this->ax[depthPhase1 - 1] != this->ax[depthPhase1]
            && this->ax[depthPhase1 - 1] != this->ax[depthPhase1] + 3
          )
        ) {

          string res;
          if (useSeparator) {
            res = solutionToString(s, depthPhase1);
          } else {
            res = solutionToString(s, -1);
          }
          auto end = std::chrono::steady_clock::now();
          std::chrono::duration<double> diff = end-start;
          std::cout << "Solution found in " << diff.count() << " s\n";
          std::cout << "Solution is made in " << s << " moves.\n";
          return res;
        }
      }
    }
  }
}

int Solver::totalDepth(int depthPhase1, int maxDepth)
{
  int mv = 0, d1 = 0, d2 = 0;
  int maxDepthPhase2 = min(10, maxDepth - depthPhase1);// Allow only max 10 moves in phase2
  for (int i = 0; i < depthPhase1; i++) {
    mv = 3 * this->ax[i] + this->po[i] - 1;
    // System.out.format("%d %d %d %d\n", i, mv, ax[i], po[i]);
    this->URFtoDLF[i + 1] = this->c.URFtoDLF_Move[this->URFtoDLF[i]][mv];
    this->FRtoBR[i + 1] = this->c.FRtoBR_Move[this->FRtoBR[i]][mv];
    this->parity[i + 1] = this->c.parityMove[this->parity[i]][mv];
  }

  if (
    (d1 = this->c.getPruning(
      this->c.Slice_URFtoDLF_Parity_Prun,
      (N_SLICE2 * this->URFtoDLF[depthPhase1] + this->FRtoBR[depthPhase1]) * 2 + this->parity[depthPhase1]
    )
  ) > maxDepthPhase2){
    return -1;
  }
  for (int i = 0; i < depthPhase1; i++) {
    mv = 3 * this->ax[i] + this->po[i] - 1;
    this->URtoUL[i + 1] = this->c.URtoUL_Move[this->URtoUL[i]][mv];
    this->UBtoDF[i + 1] = this->c.UBtoDF_Move[this->UBtoDF[i]][mv];
  }
  this->URtoDF[depthPhase1] = this->c.MergeURtoULandUBtoDF[this->URtoUL[depthPhase1]][this->UBtoDF[depthPhase1]];

  if (
    (d2 = this->c.getPruning(
      this->c.Slice_URtoDF_Parity_Prun,
      (N_SLICE2 * this->URtoDF[depthPhase1] + this->FRtoBR[depthPhase1]) * 2 + this->parity[depthPhase1]
    )
  ) > maxDepthPhase2) {
    return -1;
  }

  if ((this->minDistPhase2[depthPhase1] = max(d1, d2)) == 0) {// already solved
    return depthPhase1;
  }

  // now set up search

  int depthPhase2 = 1;
  int n = depthPhase1;
  int busy = 0;
  this->po[depthPhase1] = 0;
  this->ax[depthPhase1] = 0;
  this->minDistPhase2[n + 1] = 1;// else failure for depthPhase2=1, n=0
  // +++++++++++++++++++ end initialization +++++++++++++++++++++++++++++++++
  do {
    do {
      if ((depthPhase1 + depthPhase2 - n > this->minDistPhase2[n + 1]) && !busy) {
        if (this->ax[n] == 0 || this->ax[n] == 3)// Initialize next move
        {
          this->ax[++n] = 1;
          this->po[n] = 2;
        } else {
          this->ax[++n] = 0;
          this->po[n] = 1;
        }
      } else if ((this->ax[n] == 0 || this->ax[n] == 3) ? (++this->po[n] > 3) : ((this->po[n] = this->po[n] + 2) > 3)) {
        do {// increment axis
          if (++this->ax[n] > 5) {
            if (n == depthPhase1) {
              if (depthPhase2 >= maxDepthPhase2){
                return -1;
              } else {
                depthPhase2++;
                this->ax[n] = 0;
                this->po[n] = 1;
                busy = 0;
                break;
              }
            } else {
              n--;
              busy = 1;
              break;
            }
          } else {
            if (this->ax[n] == 0 || this->ax[n] == 3){
              this->po[n] = 1;
            } else {
              this->po[n] = 2;
              busy = 0;
            }
          }
        } while (n != depthPhase1 && (this->ax[n - 1] == this->ax[n] || this->ax[n - 1] - 3 == this->ax[n]));
      } else {
        busy = 0;
      }
    } while (busy);

    // +++++++++++++ compute new coordinates and new minDist ++++++++++
    mv = 3 * this->ax[n] + this->po[n] - 1;
    this->URFtoDLF[n + 1] = this->c.URFtoDLF_Move[this->URFtoDLF[n]][mv];
    this->FRtoBR[n + 1] = this->c.FRtoBR_Move[this->FRtoBR[n]][mv];
    this->parity[n + 1] = this->c.parityMove[this->parity[n]][mv];
    this->URtoDF[n + 1] = this->c.URtoDF_Move[this->URtoDF[n]][mv];

    this->minDistPhase2[n + 1] = max(
      this->c.getPruning(
        this->c.Slice_URtoDF_Parity_Prun,
        (N_SLICE2 * this->URtoDF[n + 1] + this->FRtoBR[n + 1]) * 2 + this->parity[n + 1]
      ),
      this->c.getPruning(
        this->c.Slice_URFtoDLF_Parity_Prun,
        (N_SLICE2 * this->URFtoDLF[n + 1] + this->FRtoBR[n + 1]) * 2 + this->parity[n + 1]
      )
    );
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  } while (this->minDistPhase2[n + 1] != 0);
  return depthPhase1 + depthPhase2;
}


string Solver::solutionToString(int length, int depthPhase1)
{
  string res = string();
  for (int i = 0; i < length; i++) {
    switch (this->ax[i]) {
      case 0:
      res += 'U';
      break;
      case 1:
      res += 'R';
      break;
      case 2:
      res += 'F';
      break;
      case 3:
      res += 'D';
      break;
      case 4:
      res += 'L';
      break;
      case 5:
      res += 'B';
      break;
    }
    switch (this->po[i]) {
      case 1:
      res += ' ';
      res += ' ';
      break;
      case 2:
      res += '2';
      res += ' ';
      break;
      case 3:
      res += '\'';
      res += ' ';
      break;
    }
    if (i == depthPhase1 - 1) {
      res += '.';
      res += ' ';
    }
  }
  return *new string(res);
}
