#include "cubie_cube_lvl.hpp"

#include <vector>
using namespace std;

CubieCubeLvl::CubieCubeLvl() {
    // cout << "CubieCubeLvl constructor"  << endl;
  this->cornerPerm = initCornerPerm();
  this->cornerOrientation = initCornerOrientation();
  this->edgePerm = initEdgePerm();
  this->edgeOrientation =  initEdgeOrientation();
}

CubieCubeLvl::~CubieCubeLvl() {
    // cout << "CubieCubeLvl destructor"  << endl;
}

vector<Corners> CubieCubeLvl::initCornerPerm() {
  vector<Corners>  cornerPerm = {
    Corners::URF,
    Corners::UFL,
    Corners::ULB,
    Corners::UBR,
    Corners::DFR,
    Corners::DLF,
    Corners::DBL,
    Corners::DRB
  };
  return cornerPerm;
}

vector<signed char> CubieCubeLvl::initCornerOrientation() {
  vector<signed char>   cornerOrientation = {0, 0, 0, 0, 0, 0, 0, 0 }; // size == 8
  return cornerOrientation;
}

vector<Edges> CubieCubeLvl::initEdgePerm() {
  vector<Edges>         edgePerm = {
    Edges::UR,
    Edges::UF,
    Edges::UL,
    Edges::UB,
    Edges::DR,
    Edges::DF,
    Edges::DL,
    Edges::DB,
    Edges::FR,
    Edges::FL,
    Edges::BL,
    Edges::BR
  };
  return edgePerm;
}

vector<signed char> CubieCubeLvl::initEdgeOrientation() {
  vector<signed char>   edgeOrientation = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // size == 12
  return edgeOrientation;
}

int CubieCubeLvl::Cnk(int n, int k) {
    int i, j, s;
    if (n < k)
        return 0;
    if (k > n / 2)
        k = n - k;
    for (s = 1, i = n, j = 1; i != n - k; i--, j++) {
        s *= i;
        s /= j;
    }
    return s;
}

void CubieCubeLvl::rotateLeft_corner(vector<Corners> &vec, int left, int right){
  Corners tmp = vec[left];
  for (int i = left; i < right; i++)
      vec[i] = vec[i + 1];
  vec[right] = tmp;
}

void CubieCubeLvl::rotateRight_corner(vector<Corners> &vec, int left, int right)
{
    Corners tmp = vec[right];
    for (int i = right; i > left; i--)
        vec[i] = vec[i - 1];
    vec[left] = tmp;
}

void CubieCubeLvl::rotateLeft_edge(vector<Edges> &vec, int left, int right)
{
    Edges tmp = vec[left];
    for (int i = left; i < right; i++)
        vec[i] = vec[i + 1];
    vec[right] = tmp;
}

void CubieCubeLvl::rotateRight_edge(vector<Edges> &vec, int left, int right)
{
    Edges tmp = vec[right];
    for (int i = right; i > left; i--)
        vec[i] = vec[i - 1];
    vec[left] = tmp;
}

FaceCube CubieCubeLvl::toFaceCube(){
  FaceCube fcRet = FaceCube();
  FaceCube ref = FaceCube();
  for(int i = 0; i < CORNER_COUNT; i++) {
      int j = (int) this->cornerPerm[i];// cornercubie with index j is at
      // cornerposition with index i
      signed char ori = this->cornerOrientation[i];// Orientation of this cubie
      for (int n = 0; n < 3; n++)
          fcRet.faceCube[(int) fcRet.cornerFacelet[i][(n + ori) % 3]] = ref.cornerColor[j][n];
  }
  for(int i = 0; i < EDGE_COUNT; i++)
  {
      int j = (int) this->edgePerm[i];// edgecubie with index j is at edgeposition
      // with index i
      signed char ori = (signed char) this->edgeOrientation[i];// Orientation of this cubie
      for (int n = 0; n < 2; n++)
          fcRet.faceCube[(int) fcRet.edgeFacelet[i][(n + ori) % 2]] = ref.edgeColor[j][n];
  }
  return fcRet;
}

void CubieCubeLvl::cornerMultiply(Move &m)
{
    vector<Corners> cornPerm = {(Corners)0, (Corners)0, (Corners)0, (Corners)0, (Corners)0, (Corners)0, (Corners)0, (Corners)0};
    vector<signed char> cornOri = {0, 0, 0, 0, 0, 0, 0, 0};


    for (int corn = 0; corn < CORNER_COUNT; corn++) {
        cornPerm[corn] = this->cornerPerm[(int)m.cornerPerm[corn]];

        signed char oriA = (signed char) this->cornerOrientation[(int)m.cornerPerm[corn]];
        signed char oriB = m.cornerOrientation[corn];
        signed char ori = 0;

        if (oriA < 3 && oriB < 3) // if both cubes are regular cubes...
        {
            ori = oriA + oriB; // just do an addition modulo 3 here
            if (ori >= 3)
                ori -= 3; // the composition is a regular cube

            // +++++++++++++++++++++not used in this implementation +++++++++++++++++++++++++++++++++++
        } else if (oriA < 3 && oriB >= 3) // if cube b is in a mirrored
        // state...
        {
            ori = oriA + oriB;
            if (ori >= 6)
                ori -= 3; // the composition is a mirrored cube
        } else if (oriA >= 3 && oriB < 3) // if cube a is an a mirrored
        // state...
        {
            ori = oriA - oriB;
            if (ori < 3)
                ori += 3; // the composition is a mirrored cube
        } else if (oriA >= 3 && oriB >= 3) // if both cubes are in mirrored
        // states...
        {
            ori = oriA - oriB;
            if (ori < 0)
                ori += 3; // the composition is a regular cube
            // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        }
        cornOri[corn] = ori;
    }
    for(int c = 0; c < CORNER_COUNT; c++) {
        this->cornerPerm[c] = cornPerm[c];
        this->cornerOrientation[c] = cornOri[c];
    }
}

void CubieCubeLvl::edgeMultiply(Move &m)
{
    vector<Edges> edgePerm = {(Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0};
    vector<signed char> edgeOri = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    for(int edge = 0; edge < EDGE_COUNT; edge++) {
        edgePerm[edge] = this->edgePerm[(int)m.edgePerm[edge]];
        edgeOri[edge] = (m.edgeOrientation[edge] + this->edgeOrientation[(int)m.edgePerm[edge]]) % 2;
    }
    for(int e = 0; e < EDGE_COUNT; e++) {
        this->edgePerm[e] = edgePerm[e];
        this->edgeOrientation[e] = edgeOri[e];
    }
}

void CubieCubeLvl::multiply(Move &m)
{
    cornerMultiply(m);
    edgeMultiply(m);
}

short CubieCubeLvl::getTwist()
{
    short ret = 0;
    for (int i = (int)Corners::URF; i < (int)Corners::DRB; i++){
        ret = (short) (3 * ret + this->cornerOrientation[i]);
      }
    return ret;
}

void CubieCubeLvl::setTwist(short twist)
{
    int twistParity = 0;
    for (int i = (int)Corners::DRB - 1; i >= (int)Corners::URF; i--) {
        twistParity += this->cornerOrientation[i] = (signed char) (twist % 3);
        twist /= 3;
    }
    this->cornerOrientation[(int)Corners::DRB] = (signed char) ((3 - twistParity % 3) % 3);
}

short  CubieCubeLvl::getFlip()
{
    short ret = 0;
    for (int i = (int)Edges::UR; i < (int)Edges::BR; i++)
        ret = (short) (2 * ret + this->edgeOrientation[i]);
    return ret;
}

void CubieCubeLvl::setFlip(short flip)
{
    int flipParity = 0;
    for (int i = (int)Edges::BR - 1; i >= (int)Edges::UR; i--) {
        flipParity += this->edgeOrientation[i] = (signed char) (flip % 2);
        flip /= 2;
    }
    this->edgeOrientation[(signed char)Edges::BR] = (signed char) ((2 - flipParity % 2) % 2);
}

short CubieCubeLvl::cornerParity()
{
    int s = 0;
    for (int i = (int)Corners::DRB; i >= (int)Corners::URF + 1; i--)
        for (int j = i - 1; j >= (int)Corners::URF; j--)
            if (this->cornerPerm[j] > this->cornerPerm[i])
                s++;
    return (short) (s % 2);
}

short CubieCubeLvl::edgeParity()
{
    int s = 0;
    for (int i = (int)Edges::BR; i >= (int)Edges::UR + 1; i--)
        for (int j = i - 1; j >= (int)Edges::UR; j--)
            if (this->edgePerm[j] > this->edgePerm[i])
                s++;
    return (short) (s % 2);
}

short CubieCubeLvl::getFRtoBR()
{
    int a = 0;
    int x = 0;
    vector<Edges> edgeFour = {(Edges)0, (Edges)0, (Edges)0, (Edges)0};
    // compute the index a < (12 choose 4) and the permutation array perm.
    for (int j = (int) Edges::BR; j >= (int)Edges::UR; j--)
        if (Edges::FR <= this->edgePerm[j] && this->edgePerm[j] <= Edges::BR) {
            a += Cnk(11 - j, x + 1);
            edgeFour[3 - x++] = this->edgePerm[j];
        }

    int b = 0;
    for (int j = 3; j > 0; j--)// compute the index b < 4! for the
    // permutation in perm
    {
        int k = 0;
        while ((int) edgeFour[j] != j + 8) {
            rotateLeft_edge(edgeFour, 0, j);
            k++;
        }
        b = (j + 1) * b + k;
    }
    return (short) (24 * a + b);
}
void CubieCubeLvl::setFRtoBR(short idx)
{
    int x;
    vector<Edges> sliceEdge = { Edges::FR, Edges::FL, Edges::BL, Edges::BR };
    vector<Edges> otherEdge = { Edges::UR, Edges::UF, Edges::UL, Edges::UB, Edges::DR, Edges::DF, Edges::DL, Edges::DB };
    int b = idx % 24; // Permutation
    int a = idx / 24; // Combination
    for (int e = 0; e < EDGE_COUNT; e++)
        this->edgePerm[e] = Edges::DB;// Use UR to invalidate all edges

    for (int j = 1, k; j < 4; j++)// generate permutation from index b
    {
        k = b % (j + 1);
        b /= j + 1;
        while (k-- > 0)
            rotateRight_edge(sliceEdge, 0, j);
    }

    x = 3;// generate combination and set slice edges
    for (int j = (int) Edges::UR; j <= (int) Edges::BR; j++)
        if (a - Cnk(11 - j, x + 1) >= 0) {
            this->edgePerm[j] = sliceEdge[3 - x];
            a -= Cnk(11 - j, x-- + 1);
        }
    x = 0; // set the remaining edges UR..DB
    for (int j = (int) Edges::UR; j <= (int) Edges::BR; j++)
        if (this->edgePerm[j] ==  Edges::DB)
            this->edgePerm[j] = otherEdge[x++];
}

short CubieCubeLvl::getURFtoDLF()
{
    int a = 0;
    int x = 0;
    vector<Corners> cornerSix = {(Corners)0, (Corners)0, (Corners)0, (Corners)0, (Corners)0, (Corners)0};
    // compute the index a < (8 choose 6) and the corner permutation.
    for (int j = (int) Corners::URF; j <= (int) Corners::DRB; j++)
        if (this->cornerPerm[j] <= Corners::DLF) {
            a += Cnk(j, x + 1);
            cornerSix[x++] = this->cornerPerm[j];
        }

    int b = 0;
    for (int j = 5; j > 0; j--)// compute the index b < 6! for the
    // permutation in corner6
    {
        int k = 0;
        while ((int)cornerSix[j] != j) {
            rotateLeft_corner(cornerSix, 0, j);
            k++;
        }
        b = (j + 1) * b + k;
    }

    return (short) (720 * a + b);
}

void CubieCubeLvl::setURFtoDLF(short idx)
{
    int x;
    vector<Corners> cornerSix = { Corners::URF, Corners::UFL, Corners::ULB, Corners::UBR, Corners::DFR, Corners::DLF};
    vector<Corners> otherCorner = { Corners::DBL, Corners::DRB };
    int b = idx % 720; // Permutation
    int a = idx / 720; // Combination
    for(int c = 0; c < CORNER_COUNT; c++)
        this->cornerPerm[c] = Corners::DRB;// Use DRB to invalidate all corners

    for (int j = 1, k; j < 6; j++)// generate permutation from index b
    {
        k = b % (j + 1);
        b /= j + 1;
        while (k-- > 0)
            rotateRight_corner(cornerSix, 0, j);
    }
    x = 5;// generate combination and set corners
    for (int j = (int)Corners::DRB; j >= 0; j--)
        if (a - Cnk(j, x + 1) >= 0) {
            this->cornerPerm[j] = cornerSix[x];
            a -= Cnk(j, x-- + 1);
        }
    x = 0;
    for (int j = (int)Corners::URF; j <= (int)Corners::DRB; j++)
        if (this->cornerPerm[j] == Corners::DRB)
            this->cornerPerm[j] = otherCorner[x++];
}

int CubieCubeLvl::getURtoDF()
{
    int a = 0, x = 0;
    vector<Edges> edgeSix = {(Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0};
    // compute the index a < (12 choose 6) and the edge permutation.
    for (int j = (int)Edges::UR; j <= (int)Edges::BR; j++)
        if (this->edgePerm[j] <= Edges::DF) {
            a += Cnk(j, x + 1);
            edgeSix[x++] = this->edgePerm[j];
        }

    int b = 0;
    for (int j = 5; j > 0; j--)// compute the index b < 6! for the
    // permutation in edgeSix
    {
        int k = 0;
        while ((int)edgeSix[j] != j) {
            rotateLeft_edge(edgeSix, 0, j);
            k++;
        }
        b = (j + 1) * b + k;
    }
    return 720 * a + b;
}

void CubieCubeLvl::setURtoDF(int idx)
{
    int x;
    vector<Edges> edgeSix = { Edges::UR, Edges::UF, Edges::UL, Edges::UB, Edges::DR, Edges::DF };
    vector<Edges> otherEdge = { Edges::DL, Edges::DB, Edges::FR, Edges::FL, Edges::BL, Edges::BR };
    int b = idx % 720; // Permutation
    int a = idx / 720; // Combination

    for(int e = 0; e < EDGE_COUNT; e++)
        this->edgePerm[e] = Edges::BR;// Use BR to invalidate all edges

    for (int j = 1, k; j < 6; j++)// generate permutation from index b
    {
        k = b % (j + 1);
        b /= j + 1;
        while (k-- > 0)
            rotateRight_edge(edgeSix, 0, j);
    }
    x = 5;// generate combination and set edges
    for (int j = (int) Edges::BR; j >= 0; j--)
        if (a - Cnk(j, x + 1) >= 0) {
            this->edgePerm[j] = edgeSix[x];
            a -= Cnk(j, x-- + 1);
        }
    x = 0; // set the remaining edges DL..BR
    for (int j = (int)Edges::UR; j <= (int)Edges::BR; j++)
        if (this->edgePerm[j] == Edges::BR)
            this->edgePerm[j] = otherEdge[x++];
}

short CubieCubeLvl::getURtoUL()
{
    int a = 0, x = 0;
    vector<Edges> edgeThree = {(Edges)0, (Edges)0, (Edges)0};
    // compute the index a < (12 choose 3) and the edge permutation.
    for (int j = (int) Edges::UR; j <= (int) Edges::BR; j++)
        if (this->edgePerm[j] <= Edges::UL) {
            a += Cnk(j, x + 1);
            edgeThree[x++] = this->edgePerm[j];
        }

    int b = 0;
    for (int j = 2; j > 0; j--)// compute the index b < 3! for the
    // permutation in edgeThree
    {
        int k = 0;
        while ((int)edgeThree[j] != j) {
            rotateLeft_edge(edgeThree, 0, j);
            k++;
        }
        b = (j + 1) * b + k;
    }

    return (short) (6 * a + b);
}

void CubieCubeLvl::setURtoUL(short idx)
{
    int x;
    vector<Edges> edgeThree = { Edges::UR, Edges::UF, Edges::UL };
    int b = idx % 6; // Permutation
    int a = idx / 6; // Combination
    for(int e = 0; e < EDGE_COUNT; e++) {
        this->edgePerm[e] = Edges::BR;// Use BR to invalidate all edges
    }

    for (int j = 1, k; j < 3; j++) {// generate permutation from index b
        k = b % (j + 1);
        b /= j + 1;
        while (k-- > 0)
            rotateRight_edge(edgeThree, 0, j);
    }
    x = 2;// generate combination and set edges
    for (int j = (int) Edges::BR; j >= 0; j--) {
        if (a - Cnk(j, x + 1) >= 0) {
            this->edgePerm[j] = edgeThree[x];
            a -= Cnk(j, x-- + 1);
        }
    }
}

short CubieCubeLvl::getUBtoDF()
{
    int a = 0, x = 0;
    vector<Edges> edgeThree = {(Edges)0, (Edges)0, (Edges)0};
    // compute the index a < (12 choose 3) and the edge permutation.
    for (int j = (int) Edges::UR; j <= (int) Edges::BR; j++)
        if (Edges::UB <= this->edgePerm[j] && this->edgePerm[j] <= Edges::DF) {
            a += Cnk(j, x + 1);
            edgeThree[x++] = this->edgePerm[j];
        }

    int b = 0;
    for (int j = 2; j > 0; j--)// compute the index b < 3! for the
    // permutation in edgeThree
    {
        int k = 0;
        while (edgeThree[j] != (Edges)((int)Edges::UB + j)) {
            rotateLeft_edge(edgeThree, 0, j);
            k++;
        }
        b = (j + 1) * b + k;
    }

    return (short) (6 * a + b);
}

void CubieCubeLvl::setUBtoDF(short idx)
{
    int x;
    vector<Edges> edgeThree = { Edges::UB, Edges::DR, Edges::DF };
    int b = idx % 6; // Permutation
    int a = idx / 6; // Combination
    for (int e = 0; e < EDGE_COUNT; e++)
        this->edgePerm[e] = Edges::BR;// Use BR to invalidate all edges

    for (int j = 1, k; j < 3; j++)// generate permutation from index b
    {
        k = b % (j + 1);
        b /= j + 1;
        while (k-- > 0)
            rotateRight_edge(edgeThree, 0, j);
    }
    x = 2;// generate combination and set edges
    for (int j = (int) Edges::BR; j >= 0; j--)
        if (a - Cnk(j, x + 1) >= 0) {
            this->edgePerm[j] = edgeThree[x];
            a -= Cnk(j, x-- + 1);
        }
}

int CubieCubeLvl::getURFtoDLB()
{
    vector<Corners> perm = {(Corners)0, (Corners)0, (Corners)0, (Corners)0, (Corners)0, (Corners)0, (Corners)0, (Corners)0};
    int b = 0;
    for (int i = 0; i < 8; i++)
        perm[i] = this->cornerPerm[i];
    for (int j = 7; j > 0; j--)// compute the index b < 8! for the permutation in perm
    {
        int k = 0;
        while ((int)perm[j] != j) {
            rotateLeft_corner(perm, 0, j);
            k++;
        }
        b = (j + 1) * b + k;
    }
    return b;
}

void CubieCubeLvl::setURFtoDLB(int idx)
{
    vector<Corners> perm = { Corners::URF, Corners::UFL, Corners::ULB, Corners::UBR, Corners::DFR, Corners::DLF, Corners::DBL, Corners::DRB };
    int k;
    for (int j = 1; j < 8; j++) {
        k = idx % (j + 1);
        idx /= j + 1;
        while (k-- > 0)
            rotateRight_corner(perm, 0, j);
    }
    int x = 7;// set corners
    for (int j = 7; j >= 0; j--)
        this->cornerPerm[j] = perm[x--];
}

int CubieCubeLvl::getURtoBR()
{
    vector<Edges> perm = {(Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0, (Edges)0};
    int b = 0;
    for (int i = 0; i < 12; i++)
        perm[i] = this->edgePerm[i];
    for (int j = 11; j > 0; j--)// compute the index b < 12! for the permutation in perm
    {
        int k = 0;
        while ((int)perm[j] != j) {
            rotateLeft_edge(perm, 0, j);
            k++;
        }
        b = (j + 1) * b + k;
    }
    return b;
}

void CubieCubeLvl::setURtoBR(int idx)
{
    vector<Edges> perm = { Edges::UR, Edges::UF, Edges::UL, Edges::UB, Edges::DR, Edges::DF, Edges::DL, Edges::DB, Edges::FR, Edges::FL, Edges::BL, Edges::BR };
    int k;
    for (int j = 1; j < 12; j++) {
        k = idx % (j + 1);
        idx /= j + 1;
        while (k-- > 0)
            rotateRight_edge(perm, 0, j);
    }
    int x = 11;// set edges
    for (int j = 11; j >= 0; j--)
        this->edgePerm[j] = perm[x--];
}

int CubieCubeLvl::verify()
{
    int sum = 0;
    int edgeCount[12] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    for(int e = 0; e < EDGE_COUNT; e++)
        edgeCount[(int)this->edgePerm[e]]++;
    for (int i = 0; i < 12; i++)
        if (edgeCount[i] != 1)
            return -2;

    for (int i = 0; i < 12; i++)
        sum += this->edgeOrientation[i];
    if (sum % 2 != 0)
        return -3;

    int cornerCount[8] = {0, 0, 0, 0, 0, 0, 0, 0};;
    for(int c = 0; c < CORNER_COUNT; c++)
        cornerCount[(int)this->cornerPerm[c]]++;
    for (int i = 0; i < 8; i++)
        if (cornerCount[i] != 1)
            return -4;// missing corners

    sum = 0;
    for (int i = 0; i < 8; i++)
        sum += this->cornerOrientation[i];
    if (sum % 3 != 0)
        return -5;// twisted corner

    if ((edgeParity() ^ cornerParity()) != 0)
        return -6;// parity error

    return 0;// cube ok
}
