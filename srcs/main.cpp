#include "defines.hpp"
#include "parser.hpp"
#include "face_cube.hpp"
#include "rubik.hpp"
#include "solver.hpp"
// #include <tclap/CmdLine.h>

using namespace std;


static void print_help(){
    cout << "usage: ./rubik [-n <value>] [\"Xy ...\"] [-h]";
    cout << endl << setw(14) << left;
    cout << "  -n <value>" << "where <value> is the number of randomly generated moves";
    cout << endl << setw(14) << left;
    cout << "  \"Xy ...\"" << "string sequence of moves separated by space";
    cout << endl << setw(16) << right;
    cout << "X " << "is one of the cube face represented by UDLRFB";
    cout << endl << setw(16) << right;
    cout << "y " << "is the movement condition:";
    cout << endl << setw(18) << right;
    cout << "\' " << "stand for anticlockwise movement";
    cout << endl << setw(18) << right;
    cout << "2 " << "stand for two clockwise movements";
    cout << endl << setw(18);
    cout << " " << "when X is alone stand for simple clockwise movement";
    cout << endl << setw(14) << left;
    cout << "  -v" << "output resolution steps";
    cout << endl << setw(14) << left;
    cout << "  -h" << "print this help";
    cout  << endl;
}

static bool commandLineParser(int argc, char **argv,bool *verbose, int *nbrMoves, string &strMoves){
  srand(time(NULL));
  if (argc == 1) {
    *nbrMoves = rand() % 50 + 5;
    return true;
  }
  for (int i = 1; i < argc; i++){
    if (strcmp(argv[i], "-h") == 0){
      print_help();
      return false;
    }
    else if (strcmp(argv[i], "-n") == 0){
      if (++i >= argc){
        print_help();
        return false;
      }
      *nbrMoves = (int)strtol(argv[i], NULL, 10);
      if (*nbrMoves <= 0){
        print_help();
        return false;
      }
    }
    else if (strcmp(argv[i], "-v") == 0){
      *verbose = true;
    }
    else {
      strMoves = string(argv[i]);
      if (strMoves.empty()){
        *nbrMoves = rand() % 50 + 5;
      }
    }

  }
  // print_help();
  return true;
}

int main(int argc, char** argv)
{
  int     nbrMoves = 0;
  string  strMoves;
  bool    verbose = false;
  if (commandLineParser(argc, argv, &verbose, &nbrMoves, strMoves)) {
    string facelets;
    FaceCube f;
    cout << "======================= Initial State =======================" << endl;

    printCubeGraphicaly(f.toString());

    try {
      Parser p = Parser(strMoves, nbrMoves);
      facelets = p.toString();

      f.setFaceCube(f.getFaceCubeFromString(facelets.c_str()));
      cout << "=======================   Init   =======================" << endl;
      CubieCubeLvl cc = f.toCubieCube();

      printCubeGraphicaly(cc.toFaceCube().toString());

      CoordLvl c(cc);
      c.initPruning("cache");
      cout << "=======================  Solving =======================" << endl;

      Solver solution;
      string sol = solution.solution(facelets, 24, 1000, 0, c, f, cc);
      cout << "======================= Solution =======================" << endl;
      if (sol.empty()) {
        std::cout << "Unsolvable cube!" << std::endl;
      }else{
        std::cout << sol << std::endl;
      }
      if (verbose){
        cout << "=======================   Steps  =======================" << endl;
        p = Parser (sol, 0);
        vector<Move> moves = getMoves();

        for (size_t i = 0; i < p.userMoves.size(); i++) {
          for (int j = 0; j < p.userMoves[i][1]; j++) {
            cc.edgeMultiply(moves[p.userMoves[i][0]]);
            cc.cornerMultiply(moves[p.userMoves[i][0]]);
          }
          cout << endl << "after move " << (i + 1) << ":" << endl;
          printCubeGraphicaly(cc.toFaceCube().toString());
        }
      }
    } catch (exception& e) {
      cout << e.what() << endl;
    }
  //
  }
  return (0);
}
