#include "face_cube.hpp"

FaceCube::FaceCube(){
  this->faceCube = getSolvedCube();
  this->cornerFacelet = initCornerFacelet();
  this->edgeFacelet = initEdgeFacelet();
  this->cornerColor = initCornerColor();
  this->edgeColor = initEdgeColor();
}

FaceCube::FaceCube(vector<int> moves) {
  (void)moves;
  this->faceCube = getSolvedCube();
  this->cornerFacelet = initCornerFacelet();
  this->edgeFacelet = initEdgeFacelet();
  this->cornerColor = initCornerColor();
  this->edgeColor = initEdgeColor();
}

vector<array<Facelets, 3>>   FaceCube::initCornerFacelet() {
  vector<array<Facelets, 3>>  cornerFacelet = {
    {{Facelets::U9, Facelets::R1, Facelets::F3 }},
    {{Facelets::U7, Facelets::F1, Facelets::L3 }},
    {{Facelets::U1, Facelets::L1, Facelets::B3 }},
    {{Facelets::U3, Facelets::B1, Facelets::R3 }},
    {{Facelets::D3, Facelets::F9, Facelets::R7 }},
    {{Facelets::D1, Facelets::L9, Facelets::F7 }},
    {{Facelets::D7, Facelets::B9, Facelets::L7 }},
    {{Facelets::D9, Facelets::R9, Facelets::B7 }}
  };
  return cornerFacelet;
}

vector<array<Facelets, 2>>             FaceCube::initEdgeFacelet() {
  vector<array<Facelets, 2>> edgeFacelet = {
    {{Facelets::U6, Facelets::R2 }},
    {{Facelets::U8, Facelets::F2 }},
    {{Facelets::U4, Facelets::L2 }},
    {{Facelets::U2, Facelets::B2 }},
    {{Facelets::D6, Facelets::R8 }},
    {{Facelets::D2, Facelets::F8 }},
    {{Facelets::D4, Facelets::L8 }},
    {{Facelets::D8, Facelets::B8 }},
    {{Facelets::F6, Facelets::R4 }},
    {{Facelets::F4, Facelets::L6 }},
    {{Facelets::B6, Facelets::L4 }},
    {{Facelets::B4, Facelets::R6 }}
  };
  return edgeFacelet;
}

vector<array<Colors, 3>>         FaceCube::initCornerColor() {
  vector<array<Colors, 3>> cornerColor ={
    {{ Colors::U, Colors::R, Colors::F }},
    {{ Colors::U, Colors::F, Colors::L }},
    {{ Colors::U, Colors::L, Colors::B }},
    {{ Colors::U, Colors::B, Colors::R }},
    {{ Colors::D, Colors::F, Colors::R }},
    {{ Colors::D, Colors::L, Colors::F }},
    {{ Colors::D, Colors::B, Colors::L }},
    {{ Colors::D, Colors::R, Colors::B }}
  };
  return cornerColor;
}

vector<array<Colors, 2>>                 FaceCube::initEdgeColor() {
  vector<array<Colors, 2>>  edgeColor = {
    {{ Colors::U, Colors::R }},
    {{ Colors::U, Colors::F }},
    {{ Colors::U, Colors::L }},
    {{ Colors::U, Colors::B }},
    {{ Colors::D, Colors::R }},
    {{ Colors::D, Colors::F }},
    {{ Colors::D, Colors::L }},
    {{ Colors::D, Colors::B }},
    {{ Colors::F, Colors::R }},
    {{ Colors::F, Colors::L }},
    {{ Colors::B, Colors::L }},
    {{ Colors::B, Colors::R }}
  };
  return edgeColor;
}

FaceCube::~FaceCube() {
  // cout << "FaceCube destructor" << endl;
}

string FaceCube::toString() {
  string str;
  for(std::vector<Colors>::iterator it = this->faceCube.begin(); it != this->faceCube.end(); ++it) {
    // Colors tmp = static_cast<Colors>(*it);
      switch(*it) {
          case Colors::U :
              str.push_back('U');
              break;
          case Colors::R :
              str.push_back('R');
              break;
          case Colors::F :
              str.push_back('F');
              break;
          case Colors::D :
              str.push_back('D');
              break;
          case Colors::L :
              str.push_back('L');
              break;
          case Colors::B :
              str.push_back('B');
              break;
      }
  }
  return (str);
}

vector<Colors> FaceCube::getFaceCubeFromString(string facelets) {
  vector<Colors> faceCube;
  for (char & c : facelets) {
    // cout << c;
    switch (c) {
      case 'U':
        faceCube.push_back(Colors::U);
        break;
      case 'R':
        faceCube.push_back(Colors::R);
        break;
      case 'F':
        faceCube.push_back(Colors::F);
        break;
      case 'D':
        faceCube.push_back(Colors::D);
        break;
      case 'L':
        faceCube.push_back(Colors::L);
        break;
      case 'B':
        faceCube.push_back(Colors::B);
        break;
    }
  }
  // cout << endl;
  return faceCube;
}

vector<Colors> FaceCube::getFaceCube() {
    return this->faceCube;
}

int FaceCube::colorsToInt(const Colors color){
  return (static_cast<int>(color));
}

void FaceCube::setFaceCube(vector<Colors> faceCube) {
  this->faceCube = faceCube;
}

vector<Colors> FaceCube::getSolvedCube() {
    vector<Colors> solvedCubeFaces;
    solvedCubeFaces.push_back(Colors::U);
    solvedCubeFaces.push_back(Colors::U);
    solvedCubeFaces.push_back(Colors::U);
    solvedCubeFaces.push_back(Colors::U);
    solvedCubeFaces.push_back(Colors::U);
    solvedCubeFaces.push_back(Colors::U);
    solvedCubeFaces.push_back(Colors::U);
    solvedCubeFaces.push_back(Colors::U);
    solvedCubeFaces.push_back(Colors::U);

    solvedCubeFaces.push_back(Colors::R);
    solvedCubeFaces.push_back(Colors::R);
    solvedCubeFaces.push_back(Colors::R);
    solvedCubeFaces.push_back(Colors::R);
    solvedCubeFaces.push_back(Colors::R);
    solvedCubeFaces.push_back(Colors::R);
    solvedCubeFaces.push_back(Colors::R);
    solvedCubeFaces.push_back(Colors::R);
    solvedCubeFaces.push_back(Colors::R);

    solvedCubeFaces.push_back(Colors::F);
    solvedCubeFaces.push_back(Colors::F);
    solvedCubeFaces.push_back(Colors::F);
    solvedCubeFaces.push_back(Colors::F);
    solvedCubeFaces.push_back(Colors::F);
    solvedCubeFaces.push_back(Colors::F);
    solvedCubeFaces.push_back(Colors::F);
    solvedCubeFaces.push_back(Colors::F);
    solvedCubeFaces.push_back(Colors::F);

    solvedCubeFaces.push_back(Colors::D);
    solvedCubeFaces.push_back(Colors::D);
    solvedCubeFaces.push_back(Colors::D);
    solvedCubeFaces.push_back(Colors::D);
    solvedCubeFaces.push_back(Colors::D);
    solvedCubeFaces.push_back(Colors::D);
    solvedCubeFaces.push_back(Colors::D);
    solvedCubeFaces.push_back(Colors::D);
    solvedCubeFaces.push_back(Colors::D);

    solvedCubeFaces.push_back(Colors::L);
    solvedCubeFaces.push_back(Colors::L);
    solvedCubeFaces.push_back(Colors::L);
    solvedCubeFaces.push_back(Colors::L);
    solvedCubeFaces.push_back(Colors::L);
    solvedCubeFaces.push_back(Colors::L);
    solvedCubeFaces.push_back(Colors::L);
    solvedCubeFaces.push_back(Colors::L);
    solvedCubeFaces.push_back(Colors::L);

    solvedCubeFaces.push_back(Colors::B);
    solvedCubeFaces.push_back(Colors::B);
    solvedCubeFaces.push_back(Colors::B);
    solvedCubeFaces.push_back(Colors::B);
    solvedCubeFaces.push_back(Colors::B);
    solvedCubeFaces.push_back(Colors::B);
    solvedCubeFaces.push_back(Colors::B);
    solvedCubeFaces.push_back(Colors::B);
    solvedCubeFaces.push_back(Colors::B);

    return solvedCubeFaces;
}


CubieCubeLvl FaceCube::toCubieCube() {
    signed char ori;
    CubieCubeLvl *ccRet = new CubieCubeLvl();

    for (vector<Corners>::iterator it = ccRet->cornerPerm.begin() ; it != ccRet->cornerPerm.end(); ++it) {
      *it = Corners::URF;
    }
    // for (int i = 0; i < 8; i++)
    //     ccRet->cp[i] = URF;// invalidate corners

    for (vector<Edges>::iterator it = ccRet->edgePerm.begin() ; it != ccRet->edgePerm.end(); ++it) {
      *it = Edges::UR;
    }
    // for (int i = 0; i < 12; i++)
    //     ccRet->ep[i] = UR;// and edges

    // color_t col1, col2;
    Colors col1, col2;

    for(int i = 0; i < CORNER_COUNT; i++) {
        // get the colors of the cubie at corner i, starting with U/D
        for (ori = 0; ori < 3; ori++) {
          int index =  (int)this->cornerFacelet[i][ori];
          if (this->faceCube[index] == Colors::U ||
          this->faceCube[ (int)this->cornerFacelet[i][ori] ] == Colors::D) {
            break;
          }
        }

        col1 = this->faceCube[(int)this->cornerFacelet[i][(ori + 1) % 3]];
        col2 = this->faceCube[(int)this->cornerFacelet[i][(ori + 2) % 3]];

        for (int j = 0; j < CORNER_COUNT; j++) {
            if (col1 == cornerColor[j][1] && col2 == cornerColor[j][2]) {
                // in cornerposition i we have cornercubie j
                ccRet->cornerPerm[i] = static_cast<Corners>(j);
                ccRet->cornerOrientation[i] = ori % 3;
                break;
            }
        }
    }


    for (int i = 0; i < EDGE_COUNT; i++) {
        for (int j = 0; j < EDGE_COUNT; j++) {
            if (this->faceCube[ (int)this->edgeFacelet[i][0] ] == this->edgeColor[j][0] &&
            this->faceCube[ (int)this->edgeFacelet[i][1] ]
            == this->edgeColor[j][1] ) {
                ccRet->edgePerm[i] = static_cast<Edges>(j);
                ccRet->edgeOrientation[i] = 0;
                break;
            }
            // if (facecube->f[edgeFacelet[i][0]] == edgeColor[j][0]
            //         && facecube->f[edgeFacelet[i][1]] == edgeColor[j][1]) {
            //     ccRet->ep[i] = j;
            //     ccRet->eo[i] = 0;
            //     break;
            // }
            if (this->faceCube[(int)this->edgeFacelet[i][0]] == this->edgeColor[j][1]
                    && this->faceCube[(int)this->edgeFacelet[i][1]] == this->edgeColor[j][0]) {
                ccRet->edgePerm[i] = static_cast<Edges>(j);
                ccRet->edgeOrientation[i] = 1;
                break;
            }
        }
    }
    return *ccRet;
}
