#include "cube.hpp"

using namespace std;

Cube::Cube(vector<int> moves){
  this->_faceCube = FaceCube(moves);
  this->_cubieCubeLvl = this->_faceCube.toCubieCube();
  this->_coordLvl = CoordLvl(this->_cubieCubeLvl);
}

Cube::~Cube(){
  // cout << "Cube destructor"  << endl;
}
