#include "coord_lvl.hpp"
using namespace std;

CoordLvl::CoordLvl(){
  // cout << "CoordLvl default constructor" << endl;
}

CoordLvl::CoordLvl(CubieCubeLvl cubie_cube_lvl) {
  PRUNING_INITED = 0;
  cubeMoves = getMoves();
  twist = cubie_cube_lvl.getTwist();
  // cout << "twist :" << twist << endl;
  flip = cubie_cube_lvl.getFlip();
  // cout << "flip :" << flip << endl;
  parity = cubie_cube_lvl.cornerParity();
  // cout << "parity :" << parity << endl;
  FRtoBR = cubie_cube_lvl.getFRtoBR();
  // cout << "FRtoBR :" << FRtoBR << endl;
  URFtoDLF = cubie_cube_lvl.getURFtoDLF();
  // cout << "URFtoDLF :" << URFtoDLF << endl;
  URtoUL = cubie_cube_lvl.getURtoUL();
  // cout << "URtoUL :" << URtoUL << endl;
  UBtoDF = cubie_cube_lvl.getUBtoDF();
  // cout << "UBtoDF :" << UBtoDF << endl;
  URtoDF = cubie_cube_lvl.getURtoDF();
  // cout << "URtoDF :" << URtoDF << endl;

}

CoordLvl::~CoordLvl() {
  // cout << "CoordLvl destructor" << endl;
}


string CoordLvl::joinPath(string dir, string fileName) {

  if (dir.length() >= 200){
    throw  string("dirname is too long.");
  } else if (fileName.length() >= 30)
  {
    throw string("fileName is too long.");
  }
  return dir + "/" + fileName;
}

int CoordLvl::checkCachedTable(string name, char *ptr, int len, string cache_dir) {
  int res = 0;
  try {
    string fileName = joinPath(cache_dir, name);
    if (access(fileName.c_str(), F_OK | R_OK) != -1) {
      readFromFile(ptr, len, fileName);
      res = 0;
    }else {
      res = 1;
    }
    return res;
  } catch (exception const& e){
    cout << "Path to cache tables is too long:" << e.what() << endl;
    return (-1);
  }
  return res;
}

void CoordLvl::readFromFile(char* ptr, int len, string name)
{
    ifstream file(name, ifstream::binary);
    if (file) {
      // read len bites of the file and stock them into ptr
      file.read(ptr, len);
      file.close();
    }
}

void CoordLvl::dump_to_file(char *ptr, int len, string name, string cache_dir)
{
    int status;
    status = mkdir(cache_dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (status == 0 || errno == EEXIST) {
      try {
        string fileName = joinPath(cache_dir, name);
        ofstream file(fileName, ofstream::binary);
        if (file){
          file.write(ptr, len);
          file.close();
        }
      } catch (exception const& e){
        cout << "Path to cache tables is too long:" << e.what() << endl;
      }
    } else {
        cout << "cannot create cache tables directory" << endl;
    }
}



void CoordLvl::initPruning(string cache_dir){
  if (PRUNING_INITED == 0){
    initTwistMove(cache_dir);
    initFlipMove(cache_dir);
    initFRtoBR_Move(cache_dir);
    initURFtoDLF_Move(cache_dir);
    initURtoDF_Move(cache_dir);
    initURtoUL_Move(cache_dir);
    initUBtoDF_Move(cache_dir);
    initMergeURtoULandUBtoDF(cache_dir);
    initSlice_URFtoDLF_Parity_Prun(cache_dir);
    initSlice_URtoDF_Parity_Prun(cache_dir);
    initSlice_Twist_Prun(cache_dir);
    initSlice_Flip_Prun(cache_dir);
  }
  PRUNING_INITED = 1;
}


void CoordLvl::initTwistMove(string cache_dir) {
  if(checkCachedTable("twistMove", (char*) twistMove, sizeof(twistMove), cache_dir) != 0) {
      CubieCubeLvl solved = CubieCubeLvl();
      for (short i = 0; i < N_TWIST; i++) {
          solved.setTwist(i);
          for (int j = 0; j < 6; j++) {
              for (int k = 0; k < 3; k++) {
                  solved.cornerMultiply( cubeMoves[j]);
                  twistMove[i][3 * j + k] = solved.getTwist();
              }
              solved.cornerMultiply(cubeMoves[j]);// 4. faceturn restores
          }
      }
      dump_to_file((char*) twistMove, sizeof(twistMove), "twistMove", cache_dir);
  }
}

void CoordLvl::initFlipMove(string cache_dir) {
  if(checkCachedTable("flipMove", (char*) flipMove, sizeof(flipMove), cache_dir) != 0) {
      CubieCubeLvl solved = CubieCubeLvl();
      for (short i = 0; i < N_FLIP; i++) {
          solved.setFlip(i);
          for (int j = 0; j < 6; j++) {
              for (int k = 0; k < 3; k++) {
                  solved.edgeMultiply(cubeMoves[j]);
                  flipMove[i][3 * j + k] = solved.getFlip();
              }
              solved.edgeMultiply(cubeMoves[j]);
          }
      }
      dump_to_file((char*) flipMove, sizeof(flipMove), "flipMove", cache_dir);
  }
}

void  CoordLvl::initFRtoBR_Move(string cache_dir) {
  if(checkCachedTable("FRtoBR_Move", (char*) FRtoBR_Move, sizeof(FRtoBR_Move), cache_dir) != 0) {
      CubieCubeLvl solved = CubieCubeLvl();
      for (short i = 0; i < N_FRtoBR; i++) {
          solved.setFRtoBR(i);
          for (int j = 0; j < 6; j++) {
              for (int k = 0; k < 3; k++) {
                  solved.edgeMultiply(cubeMoves[j]);
                  FRtoBR_Move[i][3 * j + k] = solved.getFRtoBR();
              }
              solved.edgeMultiply(cubeMoves[j]);
          }
      }
      dump_to_file((char*) FRtoBR_Move, sizeof(FRtoBR_Move), "FRtoBR_Move", cache_dir);
  }
}

void  CoordLvl::initURFtoDLF_Move(string cache_dir) {
  if(checkCachedTable("URFtoDLF_Move", (char*) URFtoDLF_Move, sizeof(URFtoDLF_Move), cache_dir) != 0) {
      CubieCubeLvl solved = CubieCubeLvl();
      for (short i = 0; i < N_URFtoDLF; i++) {
          solved.setURFtoDLF(i);
          for (int j = 0; j < 6; j++) {
              for (int k = 0; k < 3; k++) {
                  solved.cornerMultiply(cubeMoves[j]);
                  URFtoDLF_Move[i][3 * j + k] = solved.getURFtoDLF();
              }
              solved.cornerMultiply(cubeMoves[j]);
          }
      }
      dump_to_file((char*) URFtoDLF_Move, sizeof(URFtoDLF_Move), "URFtoDLF_Move", cache_dir);
  }
}

void  CoordLvl::initURtoDF_Move(string cache_dir) {
  if(checkCachedTable("URtoDF_Move", (char*) URtoDF_Move, sizeof(URtoDF_Move), cache_dir) != 0) {
      CubieCubeLvl solved = CubieCubeLvl();
      for (short i = 0; i < N_URtoDF; i++) {
          solved.setURtoDF(i);
          for (int j = 0; j < 6; j++) {
              for (int k = 0; k < 3; k++) {
                  solved.edgeMultiply(cubeMoves[j]);
                  URtoDF_Move[i][3 * j + k] = (short) solved.getURtoDF();
                  // Table values are only valid for phase 2 moves!
                  // For phase 1 moves, casting to short is not possible.
              }
              solved.edgeMultiply(cubeMoves[j]);
          }
      }
      dump_to_file((char*) URtoDF_Move, sizeof(URtoDF_Move), "URtoDF_Move", cache_dir);
  }
}

void  CoordLvl::initURtoUL_Move(string cache_dir) {
  if(checkCachedTable("URtoUL_Move", (char*) URtoUL_Move, sizeof(URtoUL_Move), cache_dir) != 0) {
      CubieCubeLvl solved = CubieCubeLvl();
      for (short i = 0; i < N_URtoUL; i++) {
          solved.setURtoUL(i);
          for (int j = 0; j < 6; j++) {
              for (int k = 0; k < 3; k++) {
                  solved.edgeMultiply(cubeMoves[j]);
                  URtoUL_Move[i][3 * j + k] = solved.getURtoUL();
              }
              solved.edgeMultiply(cubeMoves[j]);
          }
      }
      dump_to_file((char*) URtoUL_Move, sizeof(URtoUL_Move), "URtoUL_Move", cache_dir);
  }
}

void  CoordLvl::initUBtoDF_Move(string cache_dir) {
  if(checkCachedTable("UBtoDF_Move", (char*) UBtoDF_Move, sizeof(UBtoDF_Move), cache_dir) != 0) {
      CubieCubeLvl solved = CubieCubeLvl();
      for (short i = 0; i < N_UBtoDF; i++) {
          solved.setUBtoDF(i);
          for (int j = 0; j < 6; j++) {
              for (int k = 0; k < 3; k++) {
                  solved.edgeMultiply(cubeMoves[j]);
                  UBtoDF_Move[i][3 * j + k] = solved.getUBtoDF();
              }
              solved.edgeMultiply(cubeMoves[j]);
          }
      }
      dump_to_file((char*) UBtoDF_Move, sizeof(UBtoDF_Move), "UBtoDF_Move", cache_dir);
  }
}

void  CoordLvl::initMergeURtoULandUBtoDF(string cache_dir) {

  if(checkCachedTable("MergeURtoULandUBtoDF", (char*) MergeURtoULandUBtoDF, sizeof(MergeURtoULandUBtoDF), cache_dir) != 0) {
      // for i, j <336 the six edges UR,UF,UL,UB,DR,DF are not in the
      // UD-slice and the index is <20160

      for (short uRtoUL = 0; uRtoUL < 336; uRtoUL++) {
          for (short uBtoDF = 0; uBtoDF < 336; uBtoDF++) {
              MergeURtoULandUBtoDF[uRtoUL][uBtoDF] = (short) getURtoDF_standalone(uRtoUL, uBtoDF);
          }
      }
      dump_to_file((char*) MergeURtoULandUBtoDF, sizeof(MergeURtoULandUBtoDF), "MergeURtoULandUBtoDF", cache_dir);
  }
}


void  CoordLvl::initSlice_URFtoDLF_Parity_Prun(string cache_dir) {
  int depth;
  int done;
  if(checkCachedTable("Slice_URFtoDLF_Parity_Prun", (char*) Slice_URFtoDLF_Parity_Prun, sizeof(Slice_URFtoDLF_Parity_Prun), cache_dir) != 0) {
      for (int i = 0; i < N_SLICE2 * N_URFtoDLF * N_PARITY / 2; i++)
          Slice_URFtoDLF_Parity_Prun[i] = -1;
      depth = 0;
      setPruning(Slice_URFtoDLF_Parity_Prun, 0, 0);
      done = 1;
      while (done != N_SLICE2 * N_URFtoDLF * N_PARITY) {
          for (int i = 0; i < N_SLICE2 * N_URFtoDLF * N_PARITY; i++) {
              int parity = i % 2;
              int URFtoDLF = (i / 2) / N_SLICE2;
              int slice = (i / 2) % N_SLICE2;
              if (getPruning(Slice_URFtoDLF_Parity_Prun, i) == depth) {
                  for (int j = 0; j < 18; j++) {
                      int newSlice;
                      int newURFtoDLF;
                      int newParity;
                      switch (j) {
                      case 3:
                      case 5:
                      case 6:
                      case 8:
                      case 12:
                      case 14:
                      case 15:
                      case 17:
                          continue;
                      default:
                          newSlice = FRtoBR_Move[slice][j];
                          newURFtoDLF = URFtoDLF_Move[URFtoDLF][j];
                          newParity = parityMove[parity][j];
                          if (getPruning(Slice_URFtoDLF_Parity_Prun, (N_SLICE2 * newURFtoDLF + newSlice) * 2 + newParity) == 0x0f) {
                              setPruning(Slice_URFtoDLF_Parity_Prun, (N_SLICE2 * newURFtoDLF + newSlice) * 2 + newParity,
                                      (signed char) (depth + 1));
                              done++;
                          }
                      }
                  }
              }
          }
          depth++;
      }
      dump_to_file((char*) Slice_URFtoDLF_Parity_Prun, sizeof(Slice_URFtoDLF_Parity_Prun), "Slice_URFtoDLF_Parity_Prun", cache_dir);
  }
}

void  CoordLvl::initSlice_URtoDF_Parity_Prun(string cache_dir) {
  int depth;
  int done;
  if(checkCachedTable("Slice_URtoDF_Parity_Prun", (char*) Slice_URtoDF_Parity_Prun, sizeof(Slice_URtoDF_Parity_Prun), cache_dir) != 0) {
      for (int i = 0; i < N_SLICE2 * N_URtoDF * N_PARITY / 2; i++)
          Slice_URtoDF_Parity_Prun[i] = -1;
      depth = 0;
      setPruning(Slice_URtoDF_Parity_Prun, 0, 0);
      done = 1;
      while (done != N_SLICE2 * N_URtoDF * N_PARITY) {
          for (int i = 0; i < N_SLICE2 * N_URtoDF * N_PARITY; i++) {
              int parity = i % 2;
              int URtoDF = (i / 2) / N_SLICE2;
              int slice = (i / 2) % N_SLICE2;
              if (getPruning(Slice_URtoDF_Parity_Prun, i) == depth) {
                  for (int j = 0; j < 18; j++) {
                      int newSlice;
                      int newURtoDF;
                      int newParity;
                      switch (j) {
                      case 3:
                      case 5:
                      case 6:
                      case 8:
                      case 12:
                      case 14:
                      case 15:
                      case 17:
                          continue;
                      default:
                          newSlice = FRtoBR_Move[slice][j];
                          newURtoDF = URtoDF_Move[URtoDF][j];
                          newParity = parityMove[parity][j];
                          if (getPruning(Slice_URtoDF_Parity_Prun, (N_SLICE2 * newURtoDF + newSlice) * 2 + newParity) == 0x0f) {
                              setPruning(Slice_URtoDF_Parity_Prun, (N_SLICE2 * newURtoDF + newSlice) * 2 + newParity,
                                      (signed char) (depth + 1));
                              done++;
                          }
                      }
                  }
              }
          }
          depth++;
      }
      dump_to_file((char*) Slice_URtoDF_Parity_Prun, sizeof(Slice_URtoDF_Parity_Prun), "Slice_URtoDF_Parity_Prun", cache_dir);
  }
}

void  CoordLvl::initSlice_Twist_Prun(string cache_dir) {
  int depth;
  int done;
  if(checkCachedTable("Slice_Twist_Prun", (char*) Slice_Twist_Prun, sizeof(Slice_Twist_Prun), cache_dir) != 0) {
      for (int i = 0; i < N_SLICE1 * N_TWIST / 2 + 1; i++)
          Slice_Twist_Prun[i] = -1;
      depth = 0;
      setPruning(Slice_Twist_Prun, 0, 0);
      done = 1;
      while (done != N_SLICE1 * N_TWIST) {
          for (int i = 0; i < N_SLICE1 * N_TWIST; i++) {
              int twist = i / N_SLICE1, slice = i % N_SLICE1;
              if (getPruning(Slice_Twist_Prun, i) == depth) {
                  for (int j = 0; j < 18; j++) {
                      int newSlice = FRtoBR_Move[slice * 24][j] / 24;
                      int newTwist = twistMove[twist][j];
                      if (getPruning(Slice_Twist_Prun, N_SLICE1 * newTwist + newSlice) == 0x0f) {
                          setPruning(Slice_Twist_Prun, N_SLICE1 * newTwist + newSlice, (signed char) (depth + 1));
                          done++;
                      }
                  }
              }
          }
          depth++;
      }
      dump_to_file((char*) Slice_Twist_Prun, sizeof(Slice_Twist_Prun), "Slice_Twist_Prun", cache_dir);
  }
}

void  CoordLvl::initSlice_Flip_Prun(string cache_dir) {
  int depth;
  int done;
  if(checkCachedTable("Slice_Flip_Prun", (char*) Slice_Flip_Prun, sizeof(Slice_Flip_Prun), cache_dir) != 0) {
      for (int i = 0; i < N_SLICE1 * N_FLIP / 2; i++)
          Slice_Flip_Prun[i] = -1;
      depth = 0;
      setPruning(Slice_Flip_Prun, 0, 0);
      done = 1;
      while (done != N_SLICE1 * N_FLIP) {
          for (int i = 0; i < N_SLICE1 * N_FLIP; i++) {
              int flip = i / N_SLICE1, slice = i % N_SLICE1;
              if (getPruning(Slice_Flip_Prun, i) == depth) {
                  for (int j = 0; j < 18; j++) {
                      int newSlice = FRtoBR_Move[slice * 24][j] / 24;
                      int newFlip = flipMove[flip][j];
                      if (getPruning(Slice_Flip_Prun, N_SLICE1 * newFlip + newSlice) == 0x0f) {
                          setPruning(Slice_Flip_Prun, N_SLICE1 * newFlip + newSlice, (signed char) (depth + 1));
                          done++;
                      }
                  }
              }
          }
          depth++;
      }
      dump_to_file((char*) Slice_Flip_Prun, sizeof(Slice_Flip_Prun), "Slice_Flip_Prun", cache_dir);
  }
}



void CoordLvl::setPruning(signed char *table, int index, signed char value) {
  if ((index & 1) == 0)
      table[index / 2] &= 0xf0 | value;
  else
      table[index / 2] &= 0x0f | (value << 4);
  }

  // Extract pruning value
  signed char CoordLvl::getPruning(signed char *table, int index) {
  signed char res;

  if ((index & 1) == 0)
      res = (table[index / 2] & 0x0f);
  else
      res = ((table[index / 2] >> 4) & 0x0f);

  return res;
}
