#include "rubik.hpp"
using namespace std;

const char *ft_get_color(char c) {
  switch(c) {
    case 'U' :
      return YELLOW.c_str();
    case 'F' :
      return RED.c_str();
    case 'D' :
      return RESET.c_str();
    case 'R' :
      return BLUE.c_str();
    case 'L' :
      return GREEN.c_str();
    case 'B' :
      return MAGENTA.c_str();
    default:
      return RESET.c_str();
  }
}


void printCubeGraphicaly(string faceCube) {
  const char *f = faceCube.c_str();

  // up facecube
  printf("       %s%c%s %s%c%s %s%c%s\n", ft_get_color(f[0]), 'o', RESET.c_str(), ft_get_color(f[1]), 'o', RESET.c_str(), ft_get_color(f[2]), 'o', RESET.c_str());
  printf("       %s%c%s %s%c%s %s%c%s\n", ft_get_color(f[3]), 'o', RESET.c_str(), ft_get_color(f[4]), 'o', RESET.c_str(), ft_get_color(f[5]), 'o', RESET.c_str());
  printf("       %s%c%s %s%c%s %s%c%s\n\n", ft_get_color(f[6]), 'o', RESET.c_str(), ft_get_color(f[7]), 'o', RESET.c_str(), ft_get_color(f[8]), 'o', RESET.c_str());

  // facecube in this order : left, front, right, back
  printf("%s%c%s %s%c%s %s%c%s  %s%c%s %s%c%s %s%c%s  %s%c%s %s%c%s %s%c%s  %s%c%s %s%c%s %s%c%s\n", ft_get_color(f[36]), 'o', RESET.c_str(), ft_get_color(f[37]), 'o', RESET.c_str(), ft_get_color(f[38]), 'o', RESET.c_str(), ft_get_color(f[18]), 'o', RESET.c_str(), ft_get_color(f[19]), 'o', RESET.c_str(), ft_get_color(f[20]), 'o', RESET.c_str(), ft_get_color(f[9]), 'o', RESET.c_str(), ft_get_color(f[10]), 'o', RESET.c_str(), ft_get_color(f[11]), 'o', RESET.c_str(), ft_get_color(f[45]), 'o', RESET.c_str(), ft_get_color(f[46]), 'o', RESET.c_str(), ft_get_color(f[47]), 'o', RESET.c_str());
  printf("%s%c%s %s%c%s %s%c%s  %s%c%s %s%c%s %s%c%s  %s%c%s %s%c%s %s%c%s  %s%c%s %s%c%s %s%c%s\n", ft_get_color(f[39]), 'o', RESET.c_str(), ft_get_color(f[40]), 'o', RESET.c_str(), ft_get_color(f[41]), 'o', RESET.c_str(), ft_get_color(f[21]), 'o', RESET.c_str(), ft_get_color(f[22]), 'o', RESET.c_str(), ft_get_color(f[23]), 'o', RESET.c_str(), ft_get_color(f[12]), 'o', RESET.c_str(), ft_get_color(f[13]), 'o', RESET.c_str(), ft_get_color(f[14]), 'o', RESET.c_str(), ft_get_color(f[48]), 'o', RESET.c_str(), ft_get_color(f[49]), 'o', RESET.c_str(), ft_get_color(f[50]), 'o', RESET.c_str());
  printf("%s%c%s %s%c%s %s%c%s  %s%c%s %s%c%s %s%c%s  %s%c%s %s%c%s %s%c%s  %s%c%s %s%c%s %s%c%s\n\n", ft_get_color(f[42]), 'o', RESET.c_str(), ft_get_color(f[43]), 'o', RESET.c_str(), ft_get_color(f[44]), 'o', RESET.c_str(), ft_get_color(f[24]), 'o', RESET.c_str(), ft_get_color(f[25]), 'o', RESET.c_str(), ft_get_color(f[26]), 'o', RESET.c_str(), ft_get_color(f[15]), 'o', RESET.c_str(), ft_get_color(f[16]), 'o', RESET.c_str(), ft_get_color(f[17]), 'o', RESET.c_str(), ft_get_color(f[51]), 'o', RESET.c_str(), ft_get_color(f[52]), 'o', RESET.c_str(), ft_get_color(f[53]), 'o', RESET.c_str());

  // down facecube
  printf("       %s%c%s %s%c%s %s%c%s\n", ft_get_color(f[27]), 'o', RESET.c_str(), ft_get_color(f[28]), 'o', RESET.c_str(), ft_get_color(f[29]), 'o', RESET.c_str());
  printf("       %s%c%s %s%c%s %s%c%s\n", ft_get_color(f[30]), 'o', RESET.c_str(), ft_get_color(f[31]), 'o', RESET.c_str(), ft_get_color(f[32]), 'o', RESET.c_str());
  printf("       %s%c%s %s%c%s %s%c%s\n", ft_get_color(f[33]), 'o', RESET.c_str(), ft_get_color(f[34]), 'o', RESET.c_str(), ft_get_color(f[35]), 'o', RESET.c_str());
}



vector<Move> getMoves(){
      vector<Move>         cubeMoves;
      cubeMoves.push_back(Move('U'));
      cubeMoves.push_back(Move('R'));
      cubeMoves.push_back(Move('F'));
      cubeMoves.push_back(Move('D'));
      cubeMoves.push_back(Move('L'));
      cubeMoves.push_back(Move('B'));
      return cubeMoves;
}

int getURtoDF_standalone(short idx1, short idx2) {
    CubieCubeLvl *a = new CubieCubeLvl();
    CubieCubeLvl *b = new CubieCubeLvl();
    a->setURtoUL(idx1);
    b->setUBtoDF(idx2);
    for (int i = 0; i < 8; i++) {
        if (a->edgePerm[i] != Edges::BR) {
            if (b->edgePerm[i] != Edges::BR) {// collision
                return -1;
            } else {
                b->edgePerm[i] = a->edgePerm[i];
            }
        }
    }
    int res = b->getURtoDF();
    delete a;
    delete b;
    return res;
}
