# ---------------------------------------------------------------------------- #
# PROJECT DATA
# ---------------------------------------------------------------------------- #

NAME		=	rubik

# ---------------------------------------------------------------------------- #

SRCS		=	main.cpp						\
					parser.cpp					\
					cubie_cube_lvl.cpp 	\
					coord_lvl.cpp				\
					face_cube.cpp				\
					cube.cpp						\
					tools.cpp						\
					move.cpp 						\
					solver.cpp					\
# ---------------------------------------------------------------------------- #
# PROJECT CONFIGURATION
# ---------------------------------------------------------------------------- #

ROOT_DIR 	= `pwd`

CFLAGS		=	\
				-Wall -Wextra -Werror			\

# >>> REQUIRED FOR LIBRARIES >>> THINK ABOUT CHANGING THE *LIBS rules

CPPFLAGS	=	\
				-std=c++11					\
				-I $(DIRINC)					\

LDFLAGS		=	\

LDLIBS		=	\

# GLOBAL SETUP
AR			=	ar
CC			=	clang++
RM			=	rm
MKDIR		=	mkdir
MAKE		=	make

DIRSRC		=	./srcs/
DIROBJ		=	./.objs/
DIRINC		=	./incs/
DIRLIB		=	./libs/

# EXTRA COLOR AND DISPLAY FEATURE DE OUF
C_DFL		=	\033[0m
C_GRA		=	\033[30m
C_RED		=	\033[31m
C_GRE		=	\033[32m
C_YEL		=	\033[33m
C_BLUE		=	\033[34m
C_MAG		=	\033[35m
C_CYA		=	\033[36m
C_WHI		=	\033[37m

# ============================================================================ #

# ---------------------------------------------------------------------------- #
# SOURCES NORMALIZATION
# ---------------------------------------------------------------------------- #

SRC			=	$(addprefix $(DIRSRC), $(SRCS))
OBJ			=	$(addprefix $(DIROBJ), $(notdir $(SRC:.cpp=.o)))

# ---------------------------------------------------------------------------- #
# RULES
# ---------------------------------------------------------------------------- #

all			:	$(NAME)
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) build completed\n" "$(MAKE)"

$(NAME)		:	$(DIROBJ) $(OBJ) libs
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) linking objects\n" "$(CC)"
	@$(CC) $(OBJ) -o $(NAME) $(LDFLAGS) $(LDLIBS)

# ---------------------------------------------------------------------------- #
# CUSTOMISABLE RULES

libs		:

cleanlibs	:

fcleanlibs	:


# ---------------------------------------------------------------------------- #

clean		: cleanlibs
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) remove objects\n" "$(RM)"
	@$(RM) -rf $(DIROBJ)

fclean		:	fcleanlibs clean
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) remove binaries\n" "$(RM)"
	@$(RM) -f $(NAME)

re			:	fclean all

$(DIROBJ)	:
	@$(MKDIR) -p $(DIROBJ)

depend		:
	@sed -e '/^#start/,/^end/d' Makefile > .mftmp && mv .mftmp Makefile
	@printf "#start\n\n" >> Makefile
	@$(foreach s, $(SRC),																\
		printf '$$(DIROBJ)'										>> Makefile	&& \
		$(CC) -MM $(s) $(CPPFLAGS)								>> Makefile	&& \
																			\
		printf '\t\t@printf "$$(C_GRE)[ $(NAME) ] '				>> Makefile && \
		printf '[ %%-8s ]$$(C_DFL) " "$(CC)"\n'					>> Makefile && \
		printf '\t\t@printf "compiling $(s)\\n"\n'				>> Makefile	&& \
																			\
		printf '\t\t@$$(CC) -c $(s) -o '						>> Makefile	&& \
		printf '$(addprefix $(DIROBJ), $(notdir $(s:.cpp=.o))) '>> Makefile	&& \
		printf '$$(CPPFLAGS) $$(CFLAGS)\n\n'					>> Makefile	&& \
																			\
		printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) " "dep"				&& \
		printf "$(s) rule generated\n"										|| \
																			\
		(sed -e '/^#start/,$$d' Makefile > .mftmp && mv .mftmp Makefile		&& \
		printf "#start\n\n"										>> Makefile	&& \
		printf "$(C_RED)[ $(NAME) ] [ %-8s ]$(C_DFL) " "dep"				&& \
		printf "$(s) rule generation failed\n"								) \
	;)
	@printf "\n#end\n" >> Makefile

.PHONY	:	 libs

# ---------------------------------------------------------------------------- #
# AUTO-GENERATED SECTION - do not modify
# ---------------------------------------------------------------------------- #

#start

$(DIROBJ)main.o: srcs/main.cpp incs/parser.hpp incs/defines.hpp incs/face_cube.hpp \
  incs/cubie_cube_lvl.hpp incs/move.hpp incs/rubik.hpp
		@printf "$(C_GRE)[ rubik ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/main.cpp\n"
		@$(CC) -c ./srcs/main.cpp -o ./.objs/main.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)parser.o: srcs/parser.cpp incs/parser.hpp incs/defines.hpp
		@printf "$(C_GRE)[ rubik ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/parser.cpp\n"
		@$(CC) -c ./srcs/parser.cpp -o ./.objs/parser.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)cubie_cube_lvl.o: srcs/cubie_cube_lvl.cpp incs/cubie_cube_lvl.hpp \
  incs/defines.hpp incs/move.hpp incs/face_cube.hpp
		@printf "$(C_GRE)[ rubik ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/cubie_cube_lvl.cpp\n"
		@$(CC) -c ./srcs/cubie_cube_lvl.cpp -o ./.objs/cubie_cube_lvl.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)coord_lvl.o: srcs/coord_lvl.cpp incs/coord_lvl.hpp \
  incs/cubie_cube_lvl.hpp incs/defines.hpp incs/move.hpp \
  incs/face_cube.hpp incs/rubik.hpp
		@printf "$(C_GRE)[ rubik ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/coord_lvl.cpp\n"
		@$(CC) -c ./srcs/coord_lvl.cpp -o ./.objs/coord_lvl.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)face_cube.o: srcs/face_cube.cpp incs/face_cube.hpp incs/defines.hpp \
  incs/cubie_cube_lvl.hpp incs/move.hpp
		@printf "$(C_GRE)[ rubik ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/face_cube.cpp\n"
		@$(CC) -c ./srcs/face_cube.cpp -o ./.objs/face_cube.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)cube.o: srcs/cube.cpp incs/cube.hpp incs/defines.hpp incs/face_cube.hpp \
  incs/cubie_cube_lvl.hpp incs/move.hpp incs/coord_lvl.hpp \
  incs/rubik.hpp
		@printf "$(C_GRE)[ rubik ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/cube.cpp\n"
		@$(CC) -c ./srcs/cube.cpp -o ./.objs/cube.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)tools.o: srcs/tools.cpp incs/rubik.hpp incs/defines.hpp incs/move.hpp \
  incs/cubie_cube_lvl.hpp incs/face_cube.hpp
		@printf "$(C_GRE)[ rubik ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/tools.cpp\n"
		@$(CC) -c ./srcs/tools.cpp -o ./.objs/tools.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)move.o: srcs/move.cpp incs/move.hpp incs/defines.hpp
		@printf "$(C_GRE)[ rubik ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/move.cpp\n"
		@$(CC) -c ./srcs/move.cpp -o ./.objs/move.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)solver.o: srcs/solver.cpp incs/solver.hpp incs/defines.hpp \
  incs/face_cube.hpp incs/cubie_cube_lvl.hpp incs/move.hpp \
  incs/coord_lvl.hpp incs/rubik.hpp
		@printf "$(C_GRE)[ rubik ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/solver.cpp\n"
		@$(CC) -c ./srcs/solver.cpp -o ./.objs/solver.o $(CPPFLAGS) $(CFLAGS)


#end
