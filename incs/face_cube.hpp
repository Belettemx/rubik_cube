#ifndef FACE_CUBE_HPP
#define FACE_CUBE_HPP

#include <iostream>
#include <vector>
#include <array>
#include <tuple>
#include "defines.hpp"

class FaceCube;

#include "cubie_cube_lvl.hpp"

using namespace std;

class FaceCube {

  private:
    int                                         colorsToInt(const Colors color);
    vector<array<Facelets, 3>>                  initCornerFacelet();
    vector<array<Facelets, 2>>                  initEdgeFacelet();
    vector<array<Colors, 3>>                    initCornerColor();
    vector<array<Colors, 2>>                    initEdgeColor();

  public:
    vector<Colors>                                faceCube;
    vector<array<Facelets, 3>>                    cornerFacelet;
    vector<array<Facelets, 2>>                    edgeFacelet;
    vector<array<Colors, 3>>                      cornerColor;
    vector<array<Colors, 2>>                      edgeColor;

                      FaceCube();
                      FaceCube(vector<int> moves);
    virtual           ~FaceCube();
    vector<Colors>    getSolvedCube();
    string            toString();
    vector<Colors>    getFaceCube();
    void              setFaceCube(vector<Colors> faceCube);
    vector<Colors>    getFaceCubeFromString(const string faceCube);
    CubieCubeLvl      toCubieCube();

};

#endif
