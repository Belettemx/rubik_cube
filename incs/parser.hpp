#ifndef PARSER_HPP
#define PARSER_HPP
#include "defines.hpp"
#include "face_cube.hpp"
#include "rubik.hpp"
#include <map>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>


using namespace std;

class Parser {
  private:
    map<string, vector<int>>  _hash;
  public:

    vector<vector<int>>       userMoves;
    CubieCubeLvl              cubieCube;
                              Parser(string arg, int nbrMoves);
    virtual                   ~Parser();
    void                      split(const string &s, char delim, vector<string> &elems);
    vector<string>            split(const string &s, char delim);
    void                      movesToInt(vector<string> movesStrings);
    void                      applyMoves();
    string                    toString();
    void                      printMoves();
    void                      randomMoves(int nbrMoves);
    char                      toColorChar(int x);
    char                      toMovementChar(int x);
};

#endif
