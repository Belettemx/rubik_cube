#ifndef RUBIK
#define RUBIK

#include <vector>
#include "defines.hpp"
#include "move.hpp"
#include "cubie_cube_lvl.hpp"

using namespace std;

const char    *ft_get_color(char c);
void          printCubeGraphicaly(string faceCube);
vector<Move>  getMoves();
int           getURtoDF_standalone(short idx1, short idx2);

#endif
