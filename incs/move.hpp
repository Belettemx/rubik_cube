#ifndef MOVE_HPP
#define MOVE_HPP

#include <vector>
#include <iostream>
#include "defines.hpp"
#include "move.hpp"


class Move {

public:
  vector<Corners>     cornerPerm;
  vector<signed char> cornerOrientation;
  vector<Edges>       edgePerm;
  vector<signed char> edgeOrientation;
          Move(char c);
  virtual ~Move();
};

#endif
