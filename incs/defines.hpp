#ifndef DEFINES
#define DEFINES
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <time.h>
#include <stdlib.h>
using namespace std;

const string MAGENTA("\033[35m");
const string BMAGENTA("\033[37m");
const string CYAN("\033[36m");
const string BCYAN("\033[1;36m");
const string GREEN("\033[32m");
const string BGREEN("\033[1;32;43m");
const string YELLOW("\033[33m");
const string BYELLOW("\033[1;33;45m");
const string RED("\033[31m");
const string BRED("\033[1;31;43m");
const string BLUE("\033[34m");
const string BBLUE("\033[1;34;46m");
const string RESET("\033[0m");
const string C_NONE("\033[0\033[0m");
const string C_BOLD("\033[0\033[1m");
const string C_BLACK("\033[0\033[30m");
const string C_RED("\033[0\033[31m");
const string C_GREEN("\033[0\033[32m");
const string C_BROWN("\033[0\033[33m");
const string C_BLUE("\033[0\033[34m");
const string C_MAGENTA("\033[0\033[35m");
const string C_CYAN("\033[0\033[36m");
const string C_GRAY("\033[0\033[37m");

// const std::string red("\033[0;31m");
// const std::string green("\033[1;32m");
// const std::string yellow("\033[1;33m");
// const std::string cyan("\033[0;36m");
// const std::string magenta("\033[0;35m");
// const std::string reset("\033[0m");
// const std::string red("\033[0;31m");
// const std::string green("\033[1;32m");
// const std::string yellow("\033[1;33m");
// const std::string cyan("\033[0;36m");
// const std::string magenta("\033[0;35m");
// const std::string reset("\033[0m");

#define EDGE_NBR 12
#define EDGE_COUNT 12
#define FACELET_NBR 54
#define CORNER_NBR 8
#define CORNER_COUNT 8
#define FACELET_COUNT 54

// CoordLvl
#define N_TWIST     2187
#define N_FLIP      2048
#define N_SLICE1    495
#define N_SLICE2    24
#define N_PARITY    2
#define N_URFtoDLF  20160
#define N_FRtoBR    11880
#define N_URtoUL    1320
#define N_UBtoDF    1320
#define N_URtoDF    20160
#define N_URFtoDLB  40320
#define N_URtoBR    479001600
#define N_MOVE      18


enum class Colors { U, R, F, D, L, B } ;

enum class Corners {
    URF, UFL, ULB, UBR, DFR, DLF, DBL, DRB
} ;

enum class Edges {
    UR, UF, UL, UB, DR, DF, DL, DB, FR, FL, BL, BR
}  ;

enum class Facelets {
    U1, U2, U3, U4, U5, U6, U7, U8, U9, R1, R2, R3, R4, R5, R6, R7, R8, R9, F1, F2, F3, F4, F5, F6, F7, F8, F9, D1, D2, D3, D4, D5, D6, D7, D8, D9, L1, L2, L3, L4, L5, L6, L7, L8, L9, B1, B2, B3, B4, B5, B6, B7, B8, B9
} ;


#endif
