#ifndef CUBIE_CUBE_LVL_HPP
#define CUBIE_CUBE_LVL_HPP

#include <iostream>
#include "defines.hpp"
#include "move.hpp"

class CubieCubeLvl;

#include "face_cube.hpp"

using namespace std;

class CubieCubeLvl {

public:
          vector<Corners>             cornerPerm;
          vector<signed char>         cornerOrientation;
          vector<Edges>               edgePerm;
          vector<signed char>         edgeOrientation;


                      CubieCubeLvl();
  virtual             ~CubieCubeLvl();
  vector<Corners>     initCornerPerm();
  vector<signed char> initCornerOrientation();
  vector<Edges>       initEdgePerm();
  vector<signed char> initEdgeOrientation();
  int                 Cnk(int n, int k);
  void                rotateLeft_corner(vector<Corners> &vec, int left, int right);
  void                rotateRight_corner(vector<Corners> &vec, int left, int right);
  void                rotateLeft_edge(vector<Edges> &vec, int left, int right);
  void                rotateRight_edge(vector<Edges> &vec, int left, int right);
  FaceCube            toFaceCube();
  void                cornerMultiply(Move &m);
  void                edgeMultiply(Move &m);
  void                multiply(Move &m);
  // void                invCubieCube(CubieCubeLvl &c);
  short               getTwist();
  void                setTwist(short twist);
  short               getFlip();
  void                setFlip(short flip);
  short               cornerParity();
  short               edgeParity();
  short               getFRtoBR();
  void                setFRtoBR(short idx);
  short               getURFtoDLF();
  void                setURFtoDLF(short idx);
  int                 getURtoDF();
  void                setURtoDF(int idx);
  short               getURtoUL();
  void                setURtoUL(short idx);
  short               getUBtoDF();
  void                setUBtoDF(short idx);
  int                 getURFtoDLB();
  void                setURFtoDLB(int idx);
  int                 getURtoBR();
  void                setURtoBR(int idx);
  int                 verify();

};

#endif
