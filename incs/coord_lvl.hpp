#ifndef COORD_LVL_HPP
#define COORD_LVL_HPP

#include <array>
#include <fstream>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include "cubie_cube_lvl.hpp"
#include "defines.hpp"
#include "rubik.hpp"

class CoordLvl {

public:
  short twistMove[N_TWIST][N_MOVE] = {{0}};
  short flipMove[N_FLIP][N_MOVE] = {{0}};
  short parityMove[2][18] = {{ 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1 },{ 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0 }};
  short FRtoBR_Move[N_FRtoBR][N_MOVE] = {{0}};
  short URFtoDLF_Move[N_URFtoDLF][N_MOVE] = {{0}};
  short URtoDF_Move[N_URtoDF][N_MOVE] = {{0}};
  short URtoUL_Move[N_URtoUL][N_MOVE] = {{0}};
  short UBtoDF_Move[N_UBtoDF][N_MOVE] = {{0}};
  short MergeURtoULandUBtoDF[336][336] = {{0}};
  signed char Slice_URFtoDLF_Parity_Prun[N_SLICE2 * N_URFtoDLF * N_PARITY / 2] = {0};
  signed char Slice_URtoDF_Parity_Prun[N_SLICE2 * N_URtoDF * N_PARITY / 2] = {0};
  signed char Slice_Twist_Prun[N_SLICE1 * N_TWIST / 2 + 1]= {0};
  signed char Slice_Flip_Prun[N_SLICE1 * N_FLIP / 2]= {0};



  vector<Move>       cubeMoves;
  int PRUNING_INITED;

  // All coordinates are 0 for a solved cube except for UBtoDF, which is 114
  short     twist;
  short     flip;
  short     parity;
  short     FRtoBR;
  short     URFtoDLF;
  short     URtoUL;
  short     UBtoDF;
  int       URtoDF;
          CoordLvl();
          CoordLvl(CubieCubeLvl cubie_cube_lvl);
  virtual ~CoordLvl();
  void initParityMove() ;
  string joinPath(string dir, string fileName);
  int   checkCachedTable(string name, char *ptr, int len, string cache_dir);
  void  readFromFile(char* ptr, int len, string name);
  void  dump_to_file(char *ptr, int len, string name, string cache_dir);
  void  initPruning(string cache_dir);
  void  initTwistMove(string cache_dir);
  void  initFlipMove(string cache_dir);
  void  initFRtoBR_Move(string cache_dir);
  void  initURFtoDLF_Move(string cache_dir);
  void  initURtoDF_Move(string cache_dir);
  void  initURtoUL_Move(string cache_dir);
  void  initUBtoDF_Move(string cache_dir);
  void  initMergeURtoULandUBtoDF(string cache_dir);
  void  initSlice_URFtoDLF_Parity_Prun(string cache_dir);
  void  initSlice_URtoDF_Parity_Prun(string cache_dir);
  void  initSlice_Twist_Prun(string cache_dir);
  void  initSlice_Flip_Prun(string cache_dir);
  signed char getPruning(signed char *table, int index);
  void setPruning(signed char *table, int index, signed char value);

};

#endif
