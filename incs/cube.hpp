#ifndef CUBE_HPP
#define CUBE_HPP

#include <iostream>
#include "defines.hpp"
#include "face_cube.hpp"
#include "cubie_cube_lvl.hpp"
#include "coord_lvl.hpp"

using namespace std;

class Cube {
  private:
    FaceCube      _faceCube;
    CubieCubeLvl  _cubieCubeLvl;
    CoordLvl      _coordLvl;


  public:

                  Cube(vector<int> moves);
    virtual       ~Cube();


};


#endif
